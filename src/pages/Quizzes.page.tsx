import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { Table, Tooltip, Popconfirm } from 'antd';
import { ColumnsType } from 'antd/lib/table';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
import { RouteComponentProps, Link } from 'react-router-dom';

import { IRootState } from '../redux/reducers';
import { getAllQuizzes, deleteQuiz } from '../redux/actions';
import { IQuizState } from '../redux/reducers/quiz.reducers';
import { IQuiz } from '../interfaces';
import { ParsedDate, _quizService } from '../services';

export class QuizzesPage extends Component<IProps, IState> {
	// Declare constants
	columns: ColumnsType<any> = [
		{
			title: 'Subject',
			dataIndex: 'subject',
			sorter: true,
		},
		{
			title: 'Questions',
			dataIndex: 'questions',
			sorter: true,
			render: (questions) => questions.length,
		},
		{
			title: 'Created on',
			sorter: true,
			dataIndex: 'createdAt',
			render: (date) => ParsedDate(date),
		},
		{
			title: 'Edited on',
			sorter: true,
			dataIndex: 'updatedAt',
			render: (date) => ParsedDate(date),
		},
		{
			title: 'Actions',
			key: 'operation',
			fixed: 'right',
			render: (record: IQuiz) => (
				<Fragment>
					<Tooltip title='Edit'>
						<Link to={`/quiz/edit/${record._id}`} className='text-primary'>
							<EditOutlined />
						</Link>
					</Tooltip>
					<Tooltip title='Delete'>
						<Popconfirm title='Sure to delete?' okText='Delete' onConfirm={() => this.props.deleteQuiz(record._id as string)}>
							<a className='ml-4 text-danger'>
								<DeleteOutlined />
							</a>
						</Popconfirm>
					</Tooltip>
				</Fragment>
			),
		},
	];

	componentDidMount() {
		this.props.getAllQuizzes();
	}

	handleTableChange = (pagination: any, filters: any, sorter: any) => {
		console.log({ pagination, filters, sorter });
	};

	render() {
		console.log(this.props);

		// extract properties from props
		const {
			quiz: { quizzes, isLoading },
		} = this.props;

		return (
			<div className='m-4'>
				<Table columns={this.columns} rowKey='_id' dataSource={quizzes} loading={isLoading} onChange={this.handleTableChange} />
			</div>
		);
	}
}

interface IProps extends RouteComponentProps {
	getAllQuizzes: Function;
	quiz: IQuizState;
	deleteQuiz: (quizId: string) => void;
}

interface IState {}

const mapStateToProps = (state: IRootState) => ({ quiz: state.quiz });

const mapDispatchToProps = {
	getAllQuizzes,
	deleteQuiz,
};

export default connect(mapStateToProps, mapDispatchToProps)(QuizzesPage);
