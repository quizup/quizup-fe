import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { Form, Divider, Row, Col, Button, Radio, Select, notification, Card, Statistic } from 'antd';
import { RouteComponentProps } from 'react-router-dom';
import { CheckCircleTwoTone, CloseCircleTwoTone, DatabaseTwoTone } from '@ant-design/icons';

import { ISubject } from '../interfaces';
import { IRootState } from '../redux/reducers';
import { LogRender, _quizService, ShowNotification } from '../services';
import { THEME } from '../constants';
import { IQuizState } from '../redux/reducers/quiz.reducers';
import { getQuizById } from '../redux/actions';

// start - AntD Destructuring

const { Option } = Select;

// end - AntD Destructuring

// start - interfaces for current page

// end - interfaces for current page

export class AttemptQuizPage extends Component<IProps, IState> {
	state: IState = {
		subjects: [],
		subjectId: '',
		quizStarted: false,
	};

	componentDidMount() {
		_quizService
			.listSubjects()
			.then((subjects) => {
				const { data } = subjects;
				if (!data.success) throw new Error(data.error);
				this.setState({ subjects: subjects.data.data });
				console.log(subjects);
				console.log(subjects.data);
			})
			.catch((err) => {
				notification.error({ message: err.message });
			});
	}

	onFinish = (values: any) => {
		console.log(values);

		const selected = values.options;

		const _original = this.props.quiz?.quiz?.questions?.map((qst) => qst.correctOption);
		console.log(_original);

		let correct = 0;

		_original.forEach((opt, index) => {
			opt === selected[index] && correct++;
			console.log(opt, selected[index]);
		});

		console.log(correct);

		this.setState({ stats: { correct, total: _original.length, incorrect: _original.length - correct } });
	};

	onValuesChange = (values: any) => {
		console.log(values);
	};

	onChange = (value: any) => {
		console.log(`selected ${value}`);
		this.state.subjectId = value;
		// this.subjectId = value;
		// this.props.getQuizById(value);
	};

	startQuiz = () => {
		console.log(this.state.subjectId);
		if (!this.state.subjectId.trim()) {
			ShowNotification('info', 'Subject', 'Please select a subject to start quiz');
		} else {
			this.props.getQuizById(this.state.subjectId);
			this.setState({ quizStarted: true });
		}
	};

	render() {
		LogRender('AttemptQuizPage');

		// extract prop entries from props
		const {
			quiz: { quiz, isLoading },
		} = this.props;

		console.log(this.props);

		// extract entries from state
		const { subjects, quizStarted, stats } = this.state;

		return (
			<div>
				<div className='d-flex justify-content-center pt-4'>
					<Select
						showSearch
						style={{ width: '200px' }}
						placeholder='Select a subject'
						optionFilterProp='children'
						disabled={quizStarted}
						onChange={this.onChange}
						filterOption={(input, option) => {
							return option?.children.toLowerCase().indexOf(input.toLowerCase()) >= 0;
						}}>
						{subjects.map((subject) => (
							<Option key={subject._id} value={subject._id}>
								{subject.subject}
							</Option>
						))}
					</Select>
					<br></br>
					<Button onClick={this.startQuiz} disabled={quizStarted}>
						{quizStarted ? 'Quiz Started' : 'Start Quiz'}
					</Button>
				</div>

				{stats ? (
					<div className='d-flex justify-content-center mt-4'>
						<Card className='broder-0'>
							<Statistic
								title='Correct'
								value={stats.correct}
								valueStyle={{ color: '#3f8600' }}
								prefix={<CheckCircleTwoTone twoToneColor='#52c41a' />}
							/>
						</Card>
						<Card className='broder-0'>
							<Statistic
								title='Incorrect'
								value={stats.incorrect}
								valueStyle={{ color: '#3f8600' }}
								prefix={<CloseCircleTwoTone twoToneColor='#F81D22' />}
							/>
						</Card>
						<Card className='broder-0'>
							<Statistic
								title='Total'
								value={stats.total}
								valueStyle={{ color: '#3f8600' }}
								prefix={<DatabaseTwoTone twoToneColor='#7EC5FF' />}
							/>
						</Card>
					</div>
				) : (
					''
				)}

				{!isLoading && quiz?.subject ? (
					<Fragment>
						{/* Displays quiz title/langugae */}
						<Divider>Quiz: {quiz.subject}</Divider>
						<Form onFinish={this.onFinish} scrollToFirstError className='mx-5' onValuesChange={this.onValuesChange}>
							<ol>
								{/* Map questions */}

								<Form.List name='options'>
									{() =>
										quiz.questions.map((qst, qIdx) => (
											<li key={qIdx}>
												<span>{qst.description}</span>

												<Form.Item name={[qIdx]} rules={[{ required: true, message: 'Please answer this question.' }]}>
													<Radio.Group size={THEME.size}>
														{/* Map options of each question */}

														{qst.options.map((opt, oIdx) => (
															<Radio.Button key={oIdx} value={oIdx + 1}>
																{opt}
															</Radio.Button>
														))}
													</Radio.Group>
												</Form.Item>
											</li>
										))
									}
								</Form.List>
							</ol>
							<Row>
								<Col span={10}></Col>
								<Col span={4}>
									<Form.Item className='text-center'>
										<Button type='primary' block htmlType='submit'>
											Submit Test
										</Button>
									</Form.Item>
								</Col>
								<Col span={10}></Col>
							</Row>
						</Form>
					</Fragment>
				) : (
					''
				)}
			</div>
		);
	}
}

interface IProps extends RouteComponentProps {
	quiz: IQuizState;
	getQuizById: Function;
}

interface IState {
	subjects: ISubject[];
	subjectId: string;
	quizStarted: boolean;
	stats?: { total: number; correct: number; incorrect: number };
}

const mapStateToProps = (store: IRootState) => ({ quiz: store.quiz });

const mapDispatchToProps = {
	getQuizById,
};

export default connect(mapStateToProps, mapDispatchToProps)(AttemptQuizPage);
