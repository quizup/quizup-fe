import React, { Component } from 'react';
import { Form, Input, Button } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import { RouteComponentProps } from 'react-router-dom';
import { IRootState } from '../redux/reducers';
import { connect } from 'react-redux';
import { _authService } from '../services';
import { login } from '../redux/actions/user.actions';
import { IUserState } from '../redux/reducers/user.reducers';
import { ILoginDetails } from '../interfaces';

export class LoginPage extends Component<IProps, IState> {
	onFinish = (values: any) => {
		console.log('Received values of form: ', values);

		this.props.login(values);
	};

	componentDidUpdate() {
		// navigate to quizzes page if successfully logged in
		this.props.user.token && this.props.history.push('/quizzes');
	}

	render() {
		// extract values from props
		const {
			user: { isLoading },
		} = this.props;

		console.log(this.props);

		return (
			<div className='d-flex justify-content-center text-center mt-4'>
				<Form name='normal_login' className='login-form' initialValues={{ remember: true }} onFinish={this.onFinish}>
					<Form.Item name='email' rules={[{ required: true, message: 'Please input your Username!' }]}>
						<Input disabled={isLoading} prefix={<UserOutlined className='site-form-item-icon' />} placeholder='email' />
					</Form.Item>
					<Form.Item name='password' rules={[{ required: true, message: 'Please input your Password!' }]}>
						<Input.Password
							disabled={isLoading}
							prefix={<LockOutlined className='site-form-item-icon' />}
							type='password'
							placeholder='Password'
						/>
					</Form.Item>
					{/* <Form.Item>
						<Form.Item name='remember' valuePropName='checked' noStyle>
							<Checkbox>Remember me</Checkbox>
						</Form.Item>
					</Form.Item> */}

					<Form.Item>
						<Button loading={isLoading} type='primary' htmlType='submit' className='login-form-button'>
							Login
						</Button>
					</Form.Item>
				</Form>
			</div>
		);
	}
}

interface IProps extends RouteComponentProps {
	user: IUserState;
	login: (user: ILoginDetails) => void;
}

interface IState {}

const mapStateToProps = (store: IRootState) => ({ user: store.user });

const mapDipsatchToProps = {
	login,
};

export default connect(mapStateToProps, mapDipsatchToProps)(LoginPage);
