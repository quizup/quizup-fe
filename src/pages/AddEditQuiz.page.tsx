import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';
import { Form, Divider, Button, Input, Space, Tooltip, Row, Col } from 'antd';
import { FormInstance } from 'antd/lib/form/Form';

import { IRootState } from '../redux/reducers';
import { LogRender, _quizService, ShowNotification } from '../services';
import { PlusOutlined, MinusCircleOutlined } from '@ant-design/icons';
import { IQuizState } from '../redux/reducers/quiz.reducers';
import { createQuiz, getQuizById, resetQuiz } from '../redux/actions';
import { IQuiz } from '../interfaces';
import { Seed } from '../constants';

class AddEditQuizPage extends Component<IProps, IState> {
	quizFormRef = React.createRef<FormInstance>();

	// Empty question to be added at each time
	emptyQuestion = Seed.quiz.questions[0];
	emptyQuiz = Seed.quiz;

	state: IState = { isEditMode: false };

	componentDidMount() {
		console.log(this.props);

		const { params }: any = this.props.match;
		if (params.id) {
			console.log({ id: params.id });

			this.state.isEditMode = true;

			this.props.getQuizById(params.id);
		}
	}

	componentDidUpdate(prevProps: IProps, prevState: IState) {
		// Fill quiz values in from
		if (this.props.quiz !== prevProps.quiz) this.quizFormRef.current?.setFieldsValue(this.props.quiz.quiz);
		return true;
	}

	onFinish = (values: any) => {
		console.log(values);
		console.log({ ...this.props.quiz.quiz, ...values });
		const quiz = { ...this.props.quiz.quiz, ...values };
		// this.props.createQuiz(values);
		_quizService
			.createQuiz(quiz)
			.then((res) => {
				this.quizFormRef.current?.resetFields();
				ShowNotification('success', this.state.isEditMode ? 'Quiz updated' : 'Quiz created');
				this.props.resetQuiz();
				this.props.history.push('/quizzes');
			})
			.catch((err) => {
				ShowNotification('error', 'Error', err.message);
			});
	};

	onValuesChange = (values: any) => {
		console.log(values);
		console.log(this.quizFormRef.current?.getFieldsValue());
		// console.log(this.quizFormRef.current?.isFieldsTouched(true));
	};

	getOptionsLength = (qIndex: number) => this.quizFormRef.current?.getFieldsValue()?.questions?.[qIndex]?.options?.length;

	render() {
		LogRender('AddEditQuizPage');

		// extract prop entries from props
		const {
			quiz: { quiz },
		} = this.props;

		// Initialize with empty quiz if page is in AddQuiz mode

		console.log(quiz);

		return (
			<div>
				<Form
					ref={this.quizFormRef}
					initialValues={quiz}
					onFinish={this.onFinish}
					scrollToFirstError
					className='mx-5'
					onValuesChange={this.onValuesChange}>
					<Divider>
						<Form.Item name='subject' rules={[{ required: true, message: 'Type language name.' }]}>
							<Input className='text-center' placeholder='Type language name' />
						</Form.Item>
					</Divider>
					<Form.List name='questions'>
						{(questions, { add: addQuestion, remove: removeQuestion }) => {
							return (
								<Fragment>
									<ol>
										{/* Map questions */}

										{questions.map((question, qIdx) => (
											<li key={question.key}>
												<Row>
													<Col span={19}>
														<Form.Item name={[qIdx, 'description']} rules={[{ required: true, message: 'Question can not be empty.' }]}>
															<Input allowClear placeholder='Type question' />
														</Form.Item>
													</Col>
													<Col span={1}></Col>
													<Col span={1}>
														{qIdx ? (
															<Tooltip title='Remove question'>
																<MinusCircleOutlined className='text-danger' onClick={() => removeQuestion(qIdx)} />
															</Tooltip>
														) : (
															''
														)}
													</Col>
													<Col span={3}>
														<Form.Item
															name={[qIdx, 'correctOption']}
															rules={[{ required: true, message: 'Please provide correct option number.' }]}>
															<Input type='number' min='1' placeholder='Correct option' />
														</Form.Item>
														{/* <span>{[this.getOptionsLength(qIdx), question.key, question.name]}</span> */}
													</Col>
												</Row>
												<Form.List name={[qIdx, 'options']}>
													{(options, { add: addOption, remove: removeOption }) => {
														return (
															<Fragment>
																<ol className='p-4'>
																	<Row>
																		{/* Map options of each question */}

																		{options.map((option, pIdx) => (
																			<Fragment key={option.key}>
																				<Col span={5}>
																					<li>
																						<Space className='d-flex' align='start'>
																							<Form.Item
																								name={[pIdx]}
																								fieldKey={[option.key]}
																								rules={[{ required: true, message: 'Option can not be empty.' }]}>
																								<Input allowClear placeholder='Type option' />
																							</Form.Item>
																							{pIdx ? (
																								<Tooltip title='Remove option'>
																									<MinusCircleOutlined className='text-danger' onClick={() => removeOption(pIdx)} />
																								</Tooltip>
																							) : (
																								''
																							)}
																						</Space>
																					</li>
																				</Col>
																				<Col span={1}></Col>
																			</Fragment>
																		))}
																	</Row>
																</ol>
																<Row className='px-4'>
																	<Col span={4}>
																		<Form.Item>
																			<Button block type='dashed' className='border-info text-info' onClick={() => addOption('')}>
																				<PlusOutlined /> Add Option
																			</Button>
																		</Form.Item>
																	</Col>
																	<Col span={4}>
																		{/* <Form.Item
																			name={['correctOption']}
																			rules={[{ required: true, message: 'Please provide correct option number.' }]}>
																			<Input type='number' min='1' max={options?.length} placeholder='Correct option' />
																		</Form.Item> */}
																	</Col>
																	<Col span={2}></Col>
																</Row>
															</Fragment>
														);
													}}
												</Form.List>
											</li>
										))}
									</ol>
									<Form.Item className='text-center'>
										<Button type='dashed' className='border-info text-info' onClick={() => addQuestion(this.emptyQuestion)}>
											<PlusOutlined /> Add Question
										</Button>
									</Form.Item>
								</Fragment>
							);
						}}
					</Form.List>
					<Row>
						<Col span={10}></Col>
						<Col span={4}>
							<Form.Item className='text-center'>
								<Button type='primary' block htmlType='submit'>
									{this.state.isEditMode ? 'Save Edit' : 'Create Quiz'}
								</Button>
							</Form.Item>
						</Col>
						<Col span={10}></Col>
					</Row>
				</Form>
			</div>
		);
	}
}

interface IProps extends RouteComponentProps {
	quiz: IQuizState;
	createQuiz: (quiz: IQuiz) => void;
	getQuizById: (quizId: string) => void;
	resetQuiz: Function;
}

interface IState {
	isEditMode: boolean;
}

const mapStateToProps = (store: IRootState) => ({ quiz: store.quiz });

const mapDipsatchToProps = {
	createQuiz,
	getQuizById,
	resetQuiz,
};

export default connect(mapStateToProps, mapDipsatchToProps)(AddEditQuizPage);
