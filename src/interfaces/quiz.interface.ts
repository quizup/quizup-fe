import { IHttpResponse, IResponse } from './misc.interface';

export interface IQuiz extends IResponse {
	subject: string;
	questions: IQuestion[];
}

export interface IQuestion {
	description: string;
	correctOption?: number;
	options: string[];
}

export interface ISubject {
	_id: string;
	subject: string;
}

// start - Http request and response interfaces

export interface IResSubject extends IHttpResponse {
	data: ISubject[];
}

export interface IResQuiz extends IHttpResponse {
	data: IQuiz & IQuiz[];
}

// end - Http request and response interfaces
