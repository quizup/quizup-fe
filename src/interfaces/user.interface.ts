import { IHttpResponse } from './misc.interface';

export interface IUser {
	name: string;
	email: string;
}

export interface ILoginDetails {
	email: string;
	password: string;
}

export interface IResLogin extends IHttpResponse {
	data: { token: string; user: IUser };
}
