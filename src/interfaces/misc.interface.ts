export type TEmptyFunction = () => void;

export type TParamFunction = (args: any) => any;

export interface IGeneric {
	[key: string]: any;
}

export interface IHttpResponse {
	success: boolean;
	message: string;
	data: any;
	error: any;
}

export interface IResponse {
	_id?: string;
	createdAt?: string;
	updatedAt?: string;
}
