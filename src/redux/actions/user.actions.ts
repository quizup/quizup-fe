import { IResLogin, ILoginDetails } from '../../interfaces';
import { EUserActions } from '../types';
import { _quizService, ShowNotification, _authService } from '../../services';

export const login = (user: ILoginDetails) =>
	function (dispatch: Function) {
		dispatch(userReq());
		_authService
			.login(user)
			.then((res) => {
				const { data } = res;
				if (data.success) {
					ShowNotification('success', 'Login', data.message);
					dispatch(userReqOk(data.data));
				} else {
					ShowNotification('warning', 'Login', data.message);
					throw new Error(data.message);
				}
			})
			.catch((err) => dispatch(userReqFail(err)));
	};

// Generic actions for CRUD on quiz
const userReq = () => ({ type: EUserActions.LOGIN_REQ });
const userReqOk = (payload: IResLogin['data']) => ({ type: EUserActions.USER_OK, payload });
const userReqFail = (error: Error) => ({ type: EUserActions.USER_FAIL, payload: error });

export const logout = () => ({ type: EUserActions.LOGOUT_REQ });
