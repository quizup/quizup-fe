import { IQuiz } from '../../interfaces';
import { EQuizActions } from '../types';
import { _quizService, ShowNotification } from '../../services';

// export const createQuiz = (quiz: IQuiz) => ({ type: EQuizActions.CREATE_QUIZ, payload: quiz });
export const createQuiz = (quiz: IQuiz) =>
	function (dispatch: Function) {
		dispatch(crudQuizReq());
		_quizService
			.createQuiz(quiz)
			.then((res) => {
				dispatch(crudQuizReqOk(res));
				// cleanup
				dispatch(resetQuiz);
			})
			.catch((err) => dispatch(crudQuizReqFail(err)));
	};

// Generic actions for CRUD on quiz
const crudQuizReq = () => ({ type: EQuizActions.QUIZ_REQ });
const crudQuizReqOk = (payload: any) => ({ type: EQuizActions.QUIZ_REQ_OK, payload });
const crudQuizReqFail = (error: Error) => ({ type: EQuizActions.QUIZ_REQ_FAIL, payload: error });

// Get quiz by Id
export const getQuizById = (id: string) =>
	function (dispatch: Function) {
		dispatch(getQuizByIdReq());
		_quizService
			.getQuizById(id)
			.then((res) => {
				const { data } = res;
				if (data.success) {
					dispatch(getQuizByIdOk(data.data?.[0]));
				} else {
					throw new Error(data.message);
				}
			})
			.catch((err) => dispatch(getQuizByIdFail(err)));
	};
const getQuizByIdReq = () => ({ type: EQuizActions.GET_QUIZ_BY_ID_REQ });
const getQuizByIdOk = (payload: IQuiz) => ({ type: EQuizActions.GET_QUIZ_BY_ID_OK, payload });
const getQuizByIdFail = (error: Error) => ({ type: EQuizActions.GET_QUIZ_BY_ID_FAIL, payload: error });

// Get quizzes by Subject Name
export const getQuizzesBySubject = (subject: string) =>
	function (dispatch: Function) {
		dispatch(getQuizzesBySubjectReq());
		_quizService
			.getQuizBySubject(subject)
			.then((quiz) => dispatch(getQuizzesBySubjectOk(quiz.data.data)))
			.catch((err) => dispatch(getQuizzesBySubjectFail(err)));
	};
const getQuizzesBySubjectReq = () => ({ type: EQuizActions.GET_QUIZ_BY_SUBJECT_REQ });
const getQuizzesBySubjectOk = (payload: IQuiz[]) => ({ type: EQuizActions.GET_QUIZ_BY_SUBJECT_OK, payload });
const getQuizzesBySubjectFail = (error: Error) => ({ type: EQuizActions.GET_QUIZ_BY_SUBJECT_FAIL, payload: error });

// Get all quizzes
export const getAllQuizzes = () =>
	function (dispatch: Function) {
		dispatch(getAllQuizzesReq());
		_quizService
			.listQuizzes()
			.then((res) => {
				const { data } = res;
				if (data.success) {
					dispatch(getAllQuizzesOk(data.data));
				} else {
					ShowNotification('warning', 'Quizzes', data.message);
					throw new Error(data.message);
				}
			})
			.catch((err) => {
				dispatch(getAllQuizzesFail(err));
			});
	};
const getAllQuizzesReq = () => ({ type: EQuizActions.GET_ALL_QUIZZES_REQ });
const getAllQuizzesOk = (payload: IQuiz[]) => ({ type: EQuizActions.GET_ALL_QUIZZES_OK, payload });
const getAllQuizzesFail = (error: Error) => ({ type: EQuizActions.GET_ALL_QUIZZES_FAIL, payload: error });

// Delete quiz
export const deleteQuiz = (quizId: string) =>
	function (dispatch: Function) {
		dispatch(getAllQuizzesReq());
		_quizService
			.deleteQuiz(quizId)
			.then((res) => {
				const { data } = res;
				if (data.success) {
					// Fetch quizzes once again
					dispatch(getAllQuizzes());
					ShowNotification('success', 'Quiz', data.message);
				} else {
					ShowNotification('warning', 'Quiz', data.message);
					throw new Error(data.message);
				}
			})
			.catch((err) => {
				ShowNotification('error', 'Action', 'You are unathorized to perform this action');
				// Dispatch single quiz mutation action
				dispatch(crudQuizReqFail(err));
			});
	};

// Cleanup
export const resetQuiz = () => ({ type: EQuizActions.RESET_QUIZ });
