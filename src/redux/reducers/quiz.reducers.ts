import { IQuiz } from '../../interfaces';
import { EQuizActions } from '../types';
import { Seed } from '../../constants';

export interface IQuizState {
	quiz: IQuiz;
	quizzes: IQuiz[];
	isLoading: boolean;
	error: any;
}

const initialState: IQuizState = {
	quiz: { ...Seed.quiz },
	quizzes: [{ ...Seed.quiz, _id: '' }],
	isLoading: false,
	error: null,
};

export const quizReducer = (state = initialState, action: { type: EQuizActions; payload: any }): IQuizState => {
	switch (action.type) {
		case EQuizActions.GET_QUIZ:
			return { ...state };

		case EQuizActions.QUIZ_REQ:
			return { ...state, isLoading: true };

		case EQuizActions.RESET_QUIZ:
			return { ...state, quiz: { ...initialState.quiz } };

		case EQuizActions.QUIZ_REQ_OK:
			return { ...state, isLoading: false, quiz: action.payload };

		case EQuizActions.QUIZ_REQ_FAIL:
			return { ...state, isLoading: false, error: action.payload };

		case EQuizActions.GET_QUIZ_BY_ID_REQ:
			return { ...state, isLoading: true };

		case EQuizActions.GET_QUIZ_BY_ID_OK:
			return { ...state, isLoading: false, quiz: action.payload };

		case EQuizActions.GET_QUIZ_BY_ID_FAIL:
			return { ...state, isLoading: false, error: action.payload };

		case EQuizActions.GET_QUIZ_BY_SUBJECT_REQ:
			return { ...state, isLoading: true };

		case EQuizActions.GET_QUIZ_BY_SUBJECT_OK:
			return { ...state, isLoading: false, quizzes: action.payload };

		case EQuizActions.GET_QUIZ_BY_SUBJECT_FAIL:
			return { ...state, isLoading: false, error: action.payload };

		case EQuizActions.GET_ALL_QUIZZES_REQ:
			return { ...state, isLoading: true };

		case EQuizActions.GET_ALL_QUIZZES_OK:
			return { ...state, isLoading: false, quizzes: action.payload };

		case EQuizActions.GET_ALL_QUIZZES_FAIL:
			return { ...state, isLoading: false, error: action.payload };

		default:
			return state;
	}
};
