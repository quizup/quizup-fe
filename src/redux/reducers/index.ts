import { combineReducers } from 'redux';

import { quizReducer, IQuizState } from './quiz.reducers';
import { IUserState, userReducer } from './user.reducers';

export const rootReducer = combineReducers({ quiz: quizReducer, user: userReducer });

export interface IRootState {
	quiz: IQuizState;
	user: IUserState;
}
