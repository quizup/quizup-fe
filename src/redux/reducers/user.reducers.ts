import { IUser } from '../../interfaces';
import { EUserActions } from '../types';
import { HttpService } from '../../services/base.service';

export interface IUserState {
	user: IUser;
	token: string;
	isLoading: boolean;
	error: any;
}

// Check if user is already logged in
const user = localStorage.getItem('user');

const initialState: IUserState | any = (user && JSON.parse(user)) || {
	user: undefined,
	token: undefined,
};

console.log(initialState);

// Set token on page reload
HttpService.setToken(initialState.token);

export const userReducer = (state = initialState, action: { type: EUserActions; payload: any }): IUserState => {
	switch (action.type) {
		case EUserActions.LOGIN_REQ || EUserActions.LOGIN_REQ:
			return { ...state, isLoading: true };

		case EUserActions.USER_OK:
			HttpService.setToken(action.payload.token);
			localStorage.setItem('user', JSON.stringify(action.payload));
			return { ...state, isLoading: false, user: action.payload.user, token: action.payload.token };

		case EUserActions.LOGOUT_REQ:
			localStorage.clear();
			return { ...initialState };

		case EUserActions.USER_FAIL:
			HttpService.clearToken();
			return { ...state, error: action.payload };

		default:
			return state;
	}
};
