export enum EUserActions {
	LOGIN_REQ = 'LOGIN_REQ',
	LOGOUT_REQ = 'LOGOUT_REQ',
	// User generic actions
	USER_OK = 'USER_OK',
	USER_FAIL = 'USER_FAIL',
}
