import React, { useEffect, Fragment, memo, useState } from 'react';
import { Form, Input, Button, Space } from 'antd';
import { PlusOutlined, MinusCircleOutlined } from '@ant-design/icons';

import { FORM_MODES } from '../constants';
import { IQuestion, TParamFunction } from '../interfaces';
import { LogRender } from '../services';

function Question(props: IProps) {
	// extract properties from props
	const { questionIndex, mode } = props;
	LogRender('QuestionComponent');

	// set state from props
	const [question, setQuestion] = useState(props.question);

	// hooks
	useEffect(() => {
		// LogRender('QuestionComponent');

		console.log(props);

		return () => {
			console.log('exiting question');
		};
	}, []);

	// functions - to be passed as props

	// maps to be rendered
	// const optionsList =
	// 	mode === FORM_MODES.edit
	// 		? question.options.map((opt, index) => (
	// 				<Space key={opt.key} className='d-flex' align='start'>
	// 					<Form.Item
	// 						name={[questionIndex, 'options', opt.key || index]}
	// 						fieldKey={[index]}
	// 						rules={[{ required: true, message: 'Option can not be empty.' }]}>
	// 						<Input defaultValue={opt.text} placeholder='Type option' onChange={(e) => changeHandler(index, e.target.value)} />
	// 					</Form.Item>
	// 					<Tooltip title='Remove option'>
	// 						<MinusCircleOutlined onClick={() => removeOption(index)} />
	// 					</Tooltip>
	// 				</Space>
	// 		  ))
	// 		: question.options.map((opt, index) => (
	// 				<Radio.Button key={index} value={opt}>
	// 					{opt}
	// 				</Radio.Button>
	// 		  ));

	return (
		<div>
			{/* check if question is in edit mode and change the view accordingly */}
			{mode == FORM_MODES.edit ? (
				<Fragment>
					{/* <Space className='d-flex' align='start'>
						<Form.Item name={[questionIndex, 'description']} rules={[{ required: true, message: 'Question can not be empty.' }]}>
							<Input defaultValue={question.description} allowClear placeholder='Type question' />
						</Form.Item>
						<Tooltip title='Remove question'>
							<MinusCircleOutlined onClick={() => removeQuestion(questionIndex)} />
						</Tooltip>
					</Space>
					<br />

					{optionsList}

					<Form.Item>
						<Button type='dashed' onClick={addOption}>
							<PlusOutlined /> Add Option
						</Button>
					</Form.Item> */}

					<Form.List name='options'>
						{(fields: any[], { add, remove }: any) => {
							console.log(fields);
							return (
								<div>
									{fields.map((opt, index) => (
										<Space key={opt.key} style={{ display: 'flex', marginBottom: 8 }} align='start'>
											<Form.Item
												name={[index, 'options']}
												fieldKey={opt.key}
												rules={[{ required: true, message: 'Option can not be empty.' }]}>
												<Input defaultValue={opt.text} placeholder='Type option' />
											</Form.Item>

											<MinusCircleOutlined
												onClick={() => {
													remove(opt.name);
												}}
											/>
										</Space>
									))}

									<Form.Item>
										<Button
											type='dashed'
											onClick={() => {
												add();
											}}
											block>
											<PlusOutlined /> Add field
										</Button>
									</Form.Item>
								</div>
							);
						}}
					</Form.List>
				</Fragment>
			) : (
				<Fragment>
					<span>{question.description}</span>
					<br />
					<Form.Item
						name={questionIndex}
						fieldKey={questionIndex}
						isListField
						rules={[{ required: true, message: 'Please answer this question.' }]}>
						{/* <Radio.Group size={THEME.size}>{optionsList}</Radio.Group> */}
					</Form.Item>
				</Fragment>
			)}
		</div>
	);
}

interface IProps {
	question: IQuestion;
	questionIndex: number;
	removeQuestion: TParamFunction;
	mode?: FORM_MODES;
}

function arePropsEqual(prevProps: IProps, nextProps: IProps) {
	console.log(prevProps);
	console.log(nextProps);

	console.log(prevProps === nextProps);
	console.log(prevProps.question === nextProps.question);
	console.log(prevProps.questionIndex === nextProps.questionIndex);
	console.log(prevProps.mode === nextProps.mode);
	return false;
	// return prevProps.label === nextProps.label;
}

export default memo(Question, arePropsEqual);
