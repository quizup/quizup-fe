import { SizeType } from 'antd/lib/config-provider/SizeContext';

export const THEME = {
	size: 'small' as SizeType,
};
