import React, { Fragment } from 'react';
import { BrowserRouter as Router, Switch, Route, Link, RouteComponentProps, Redirect, NavLink } from 'react-router-dom';
import { CodeTwoTone, LoginOutlined, LogoutOutlined } from '@ant-design/icons';
import { Layout, Tooltip, Menu } from 'antd';
import { connect } from 'react-redux';

import './App.css';
import LoginPage from './pages/Login.page';
import AddEditQuizPage from './pages/AddEditQuiz.page';
import AttemptQuizPage from './pages/AttemptQuiz.page';
import QuizzesPage from './pages/Quizzes.page';
import { IRootState } from './redux/reducers';
import { IUserState } from './redux/reducers/user.reducers';
import { logout } from './redux/actions/user.actions';

function App(props: IProps | any) {
	console.log(props);

	const isLoggedIn = props?.user?.token ? true : false;

	return (
		<Fragment>
			<Router>
				<Layout.Header className='d-flex'>
					<Link to='/quiz'>
						<CodeTwoTone twoToneColor='#fed03d' style={{ fontSize: '40px' }} />
					</Link>
					{isLoggedIn ? (
						<Fragment>
							<NavLink to='/quizzes' exact className='mx-4 px-2 text-white' activeClassName='bg-warning'>
								View Quizzes
							</NavLink>
							<NavLink to='/quiz/add' exact className='mx-4 px-2 text-white' activeClassName='bg-warning'>
								Add Quiz
							</NavLink>
						</Fragment>
					) : (
						''
					)}
					<NavLink to='/quiz' exact className='px-2 text-white' activeClassName='bg-warning'>
						Attempt Quiz
					</NavLink>
					{isLoggedIn ? (
						<Link
							to='/'
							onClick={() => {
								props.logout();
								props.history?.push('/quiz');
							}}
							className='ml-auto'>
							<Tooltip title='Logout'>
								<LogoutOutlined style={{ fontSize: '30px' }} className='text-warning' />
							</Tooltip>
						</Link>
					) : (
						<Link to='/admin/login' className='ml-auto'>
							<Tooltip title='Login'>
								<LoginOutlined style={{ fontSize: '30px' }} className='text-warning' />
							</Tooltip>
						</Link>
					)}
				</Layout.Header>
				<Switch>
					<Route path='/' exact component={AttemptQuizPage} />
					<Route path='/quiz' exact component={AttemptQuizPage} />
					<Route path='/quiz/edit/:id' exact component={AddEditQuizPage} />
					<Route path='/quiz/add' exact component={AddEditQuizPage} />
					<Route path='/quizzes' exact component={QuizzesPage} />
					<Route path='/admin/login' exact component={LoginPage} />
				</Switch>
			</Router>
		</Fragment>
	);
}

interface IProps extends RouteComponentProps {
	user: IUserState;
	logout: Function;
}

const mapStateToProps = (store: IRootState) => ({ user: store.user });

const mapDispatchToProps = {
	logout,
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
