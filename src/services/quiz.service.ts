import { AxiosResponse } from 'axios';

import { HttpService } from './base.service';
import { IResSubject, IResQuiz, IQuiz, IHttpResponse } from '../interfaces';

class QuizService extends HttpService {
	private readonly prefix = 'quiz';

	listQuizzes = (): Promise<AxiosResponse<IResQuiz>> => this.get(this.prefix);

	deleteQuiz = (id: string): Promise<AxiosResponse<IHttpResponse>> => this.delete(this.prefix + '/' + id);

	listSubjects = (): Promise<AxiosResponse<IResSubject>> => this.get(this.prefix + '/subjects');

	getQuizBySubject = (subject: string): Promise<AxiosResponse<IResQuiz>> => this.get(this.prefix + '/subject/' + subject);

	getQuizById = (id: string): Promise<AxiosResponse<IResQuiz>> => this.get(this.prefix + '/id/' + id);

	createQuiz = (quiz: IQuiz): Promise<AxiosResponse<IResQuiz>> => this.post(this.prefix, quiz);
}

export const _quizService = new QuizService();
