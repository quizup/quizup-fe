import shortId from 'shortid';
import { notification } from 'antd';
import { ArgsProps } from 'antd/lib/notification';

export const LogRender = (name: string) => console.log(`%c render: ${name}! `, 'background: #222; color: #bada55; font-size: 20px;');

export const UniqueId = () => shortId.generate();

export const ShowNotification = (
	type: 'success' | 'error' | 'info' | 'warning' | 'warn' | 'open' | 'close',
	title: string,
	description = '',
	duration = 3
) => {
	notification[type]({
		message: title,
		description,
		duration,
	} as ArgsProps & string);
};

export const ParsedDate = (date: string) => {
	const _date = new Date(date);
	// return _date.toLocaleDateString(); // DD/MM/YYYY
	return _date.toLocaleString(); // DD/MM/YYYY, HH:MM:SS AM/PM
};
