import { AxiosResponse } from 'axios';

import { HttpService } from './base.service';
import { ILoginDetails, IUser, IResLogin, IHttpResponse } from '../interfaces';

class AuthService extends HttpService {
	private readonly prefix = 'user';

	login = (credentials: ILoginDetails): Promise<AxiosResponse<IResLogin>> => this.post(this.prefix + '/login', credentials);

	logout = (): Promise<AxiosResponse<IHttpResponse>> => this.get(this.prefix + '/logout');

	register = (user: IUser): Promise<AxiosResponse<any>> => this.post(this.prefix + '/regiset', user);

	whoAmI = (): Promise<AxiosResponse<any>> => this.get(this.prefix + '/whoami');
}

export const _authService = new AuthService();
